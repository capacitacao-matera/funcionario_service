package com.capacitacaomatera.funcionario_service.interfaces;

public interface IValidador {

	boolean validar();
	
}
