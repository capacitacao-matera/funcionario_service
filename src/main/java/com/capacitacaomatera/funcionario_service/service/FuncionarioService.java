package com.capacitacaomatera.funcionario_service.service;

import com.capacitacaomatera.funcionario_service.entity.Funcionario;

import java.util.List;

public interface FuncionarioService {

    void inserirFuncionario(Funcionario funcionario);

    void removerFuncionario(Long id);

    List<Funcionario> listarFuncionarios();

    Funcionario getFuncionario(Long id);
}
