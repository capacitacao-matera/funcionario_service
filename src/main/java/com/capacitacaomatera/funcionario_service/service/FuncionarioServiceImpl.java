package com.capacitacaomatera.funcionario_service.service;

import com.capacitacaomatera.funcionario_service.entity.Funcionario;
import com.capacitacaomatera.funcionario_service.exceptions.BadRequestException;
import com.capacitacaomatera.funcionario_service.exceptions.DuplicateEmployeeException;
import com.capacitacaomatera.funcionario_service.exceptions.ResourceNotFoundException;
import com.capacitacaomatera.funcionario_service.repository.FuncionarioRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FuncionarioServiceImpl implements FuncionarioService{

    private static final Logger LOGGER = LoggerFactory.getLogger(FuncionarioServiceImpl.class);

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private FuncionarioRepository funcionarioRepository;

    public void inserirFuncionario(final Funcionario funcionario) {

        if (funcionario.validar()) {
            try {
                funcionario.setSenha(passwordEncoder.encode(funcionario.getSenha()));
                funcionarioRepository.save(funcionario);
            }
            catch (DataIntegrityViolationException e){
                LOGGER.error(e.toString(), e);
                throw new DuplicateEmployeeException("Funcionário já cadastrado");
            }
        }
        else {
            throw new BadRequestException("Não foi possível registrar o funcionário. Verifique as informações do funcionário e tente novamente.");
        }

    }

    @Override
    public void removerFuncionario(final Long id) {
        try {
            funcionarioRepository.deleteById(id);
        }
        catch(EmptyResultDataAccessException e){
            throw new ResourceNotFoundException("Funcionário não existe");
        }
    }

    @Override
    public List<Funcionario> listarFuncionarios() {
        return funcionarioRepository.findAll();
    }

    @Override
    public Funcionario getFuncionario(Long id) {
        Optional<Funcionario> fRef = funcionarioRepository.findById(id);

        if(fRef.isPresent())
            return fRef.get();
        else
            throw new ResourceNotFoundException("Funcionário não encontrado.");
    }

}
