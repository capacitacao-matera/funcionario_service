package com.capacitacaomatera.funcionario_service.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class FuncionarioDTO {

    private Long id;

    private String nome;

    private String usuario;

    private String email;

    private String telefone;

    private String cpf;

    private String rg;

    @JsonIgnore
    private String senha;

    private EnderecoDTO endereco;

    public FuncionarioDTO(){/*Model Mapper*/}

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public String getRg() {
        return rg;
    }

    public EnderecoDTO getEndereco() {
        return endereco;
    }

    @JsonIgnore
    public String getSenha() {
        return senha;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public void setEndereco(EnderecoDTO endereco) {
        this.endereco = endereco;
    }

    @JsonProperty
    public void setSenha(String senha) {
        this.senha = senha;
    }
}
