package com.capacitacaomatera.funcionario_service.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ValidationException;
import java.sql.Timestamp;

@ControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RestResponseEntityExceptionHandler.class);

    @ExceptionHandler(value = {DuplicateEmployeeException.class})
    @ResponseStatus(value = HttpStatus.CONFLICT)
    protected ResponseEntity<Object> handleDuplicateEmployee (RuntimeException ex, WebRequest request) {
        return handle(
            ex,
            request,
            HttpStatus.CONFLICT
        );
    }

    @ExceptionHandler(value ={ResourceNotFoundException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    protected ResponseEntity<Object> handleResourceNotFound (RuntimeException ex, WebRequest request) {
        return handle(
            ex,
            request,
            HttpStatus.NOT_FOUND
        );
    }

    @ExceptionHandler(value = {ValidationException.class, BadRequestException.class})
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    protected ResponseEntity<Object> handleBadRequestException (RuntimeException ex, WebRequest request) {
        return handle(
            ex,
            request,
            HttpStatus.BAD_REQUEST
        );
    }

    @ExceptionHandler(value = { Exception.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    protected  ResponseEntity<Object> handleInternalError (Exception ex, WebRequest request) {
        return handle(
            ex,
            request,
            HttpStatus.INTERNAL_SERVER_ERROR
        );
    }

    private ResponseEntity<Object> handle (final Exception ex, final WebRequest request, final HttpStatus status) {
        String message = ex.toString();
        LOG.error(message, ex);

        return handleExceptionInternal(
            ex,
            new ErrorResponseObject(status.value(), ex.getMessage(), new Timestamp(System.currentTimeMillis())),
            new HttpHeaders(),
            status,
            request
        );
    }

}
