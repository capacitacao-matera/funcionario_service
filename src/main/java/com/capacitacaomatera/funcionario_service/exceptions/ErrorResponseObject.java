package com.capacitacaomatera.funcionario_service.exceptions;

import java.sql.Timestamp;

public class ErrorResponseObject {

    private int statusCode;
    private String mensagem;
    private Timestamp timestamp;

    public ErrorResponseObject(final int statusCode, final String mensagem, final Timestamp timestamp) {
        this.statusCode = statusCode;
        this.mensagem = mensagem;
        this.timestamp = timestamp;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public String getMensagem() {
        return mensagem;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }


}

