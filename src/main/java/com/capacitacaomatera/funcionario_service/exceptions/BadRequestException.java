package com.capacitacaomatera.funcionario_service.exceptions;

public class BadRequestException extends RuntimeException {

    public BadRequestException (String msg) {
        super(msg);
    }

}
