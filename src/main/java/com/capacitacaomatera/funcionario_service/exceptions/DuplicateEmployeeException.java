package com.capacitacaomatera.funcionario_service.exceptions;

public class DuplicateEmployeeException extends RuntimeException {

	public DuplicateEmployeeException(String msg) {
		super(msg);
	}

}
