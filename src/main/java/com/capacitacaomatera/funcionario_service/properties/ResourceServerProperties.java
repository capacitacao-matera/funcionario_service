package com.capacitacaomatera.funcionario_service.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("resource-server")
public class ResourceServerProperties {

    private String checkTokenEndpoint;
    private String clientId;
    private String clientSecret;

    public String getCheckTokenEndpoint() {
        return checkTokenEndpoint;
    }

    public String getClientId() {
        return clientId;
    }

    public String getClientSecret() {
        return clientSecret;
    }

    public void setCheckTokenEndpoint(String checkTokenEndpoint) {
        this.checkTokenEndpoint = checkTokenEndpoint;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }
}
