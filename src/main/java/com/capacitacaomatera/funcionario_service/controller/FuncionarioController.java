package com.capacitacaomatera.funcionario_service.controller;

import com.capacitacaomatera.funcionario_service.dto.FuncionarioDTO;
import com.capacitacaomatera.funcionario_service.entity.Funcionario;
import com.capacitacaomatera.funcionario_service.service.FuncionarioService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@CrossOrigin
@RestController
@RequestMapping("/funcionarios")
public class FuncionarioController {

    @Autowired
    private FuncionarioService funcionarioService;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping(produces = "application/json")
    public ResponseEntity<List<FuncionarioDTO>> listarFuncionarios () {

        List<Funcionario> funcionarios = funcionarioService.listarFuncionarios();
        List<FuncionarioDTO> dtos = funcionarios.stream()
            .map(this::convertToDto)
            .collect(Collectors.toList());

        if(!dtos.isEmpty())
            return  new ResponseEntity<>(dtos, HttpStatus.OK);
        else
            return  new ResponseEntity<>(dtos, HttpStatus.NO_CONTENT);
    }

    @GetMapping(value="/{id}", produces = "application/json")
    public ResponseEntity<FuncionarioDTO> getFuncionario(@PathVariable Long id) {
        return new ResponseEntity<>(convertToDto(funcionarioService.getFuncionario(id)), HttpStatus.OK);
    }

    @PostMapping(consumes="application/json")
    public ResponseEntity inserirFuncionario (@RequestBody FuncionarioDTO funcionario) {
        funcionarioService.inserirFuncionario(convertToEntity(funcionario));
        return new ResponseEntity(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> removerFuncionario (@PathVariable Long id) {
        funcionarioService.removerFuncionario(id);
        return new ResponseEntity(HttpStatus.OK);
    }

    private FuncionarioDTO convertToDto(Funcionario funcionario) {
        return modelMapper.map(funcionario, FuncionarioDTO.class);
    }

    private Funcionario convertToEntity(FuncionarioDTO funcionarioDto ) {
        return modelMapper.map(funcionarioDto, Funcionario.class);
    }
}
