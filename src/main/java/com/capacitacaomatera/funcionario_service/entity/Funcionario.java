package com.capacitacaomatera.funcionario_service.entity;

import com.capacitacaomatera.funcionario_service.interfaces.IValidador;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.ResultCheckStyle;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.ValidationException;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@SQLDelete(sql="UPDATE funcionario SET deleted = 1, updated_at = NOW() where id = ?", check = ResultCheckStyle.COUNT)
@Where(clause = "deleted = 0")
@Table(name="funcionario", uniqueConstraints={@UniqueConstraint(columnNames={"cpf", "deleted", "updatedAt"})})
@JsonIgnoreProperties({"updatedAt", "deleted"})
public class Funcionario implements IValidador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max=100)
    private String nome = null;

    @NotNull
    @Size(max = 6)
    private String usuario = null;

    @NotNull
    @Size(max = 256)
    @Email
    private String email = null;

    @NotNull
    @Size(max = 15)
    private String telefone = null;

    @NotNull
    @Size(max = 14)
    private String cpf = null;

    @NotNull
    @Size(max = 12)
    private String rg = null;

    @NotNull
    @Size(max = 60)
    private String senha = null;

    @NotNull
    @Column(columnDefinition = "varchar(50) default 'ND'")
    private String updatedAt = "ND";

    @NotNull
    @Column(columnDefinition = "boolean default false")
    private boolean deleted = false;

    @PreRemove
    public void deleteFuncionario() {
        this.updatedAt = String.valueOf(System.currentTimeMillis());
    }

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST, optional = false, targetEntity = Endereco.class)
    private Endereco endereco;

    public Funcionario() {/*Model Mapper e Hibernate*/}

    public Long getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }

    public String getUsuario() {
        return usuario;
    }

    public String getEmail() {
        return email;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getCpf() {
        return cpf;
    }

    public String getRg() {
        return rg;
    }

    public Endereco getEndereco() {
        return endereco;
    }

    public String getSenha() {
        return senha;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setRg(String rg) {
        this.rg = rg;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    @Override
    public boolean validar() {
        if (StringUtils.isBlank(this.nome))
            throw new ValidationException("Nome do funcionário deve ser informado.");

        if (StringUtils.isBlank(this.usuario))
            throw new ValidationException("Nome de usuário do funcionário deve ser informado.");

        if (StringUtils.isBlank(this.cpf))
            throw new ValidationException("CPF do funcionário deve ser informado.");

        if (StringUtils.isBlank(this.rg))
            throw new ValidationException("RG do funcionário deve ser informado.");

        if (StringUtils.isBlank(this.email))
            throw new ValidationException("E-mail do funcionário deve ser informado.");

        if (StringUtils.isBlank(this.telefone))
            throw new ValidationException("Telefone do funcionário deve ser informado.");

        if (StringUtils.isBlank(this.senha))
            throw new ValidationException("A senha do funcionário deve ser informada.");

        if(this.endereco == null)
            throw new ValidationException("Os dados de endereço do funcionário deve ser informado.");

        return this.endereco.validar();

    }

    public static final class Builder {

        private String nome;
        private String usuario;
        private String email;
        private String telefone;
        private String cpf;
        private String rg;
        private Endereco endereco;
        private String senha;

        public Builder withNome(String nome) {
            this.nome = nome;
            return this;
        }

        public Builder withUsuario(String usuario) {
            this.usuario = usuario;
            return this;
        }

        public Builder withEmail(String email) {
            this.email = email;
            return this;
        }

        public Builder withTelefone(String telefone) {
            this.telefone = telefone;
            return this;
        }

        public Builder withCpf(String cpf) {
            this.cpf = cpf;
            return this;
        }

        public Builder withRg(String rg) {
            this.rg = rg;
            return this;
        }

        public Builder withEndereco (Endereco e){
            this.endereco = e;
            return this;
        }

        public Builder withSenha(String senha) {
            this.senha = senha;
            return this;
        }

        public Funcionario build() {
            Funcionario f = new Funcionario();

            f.nome = nome;
            f.usuario = usuario;
            f.email = email;
            f.telefone = telefone;
            f.cpf = cpf;
            f.rg = rg;
            f.endereco = endereco;
            f.senha = senha;

            return f;
        }
    }
}
