package com.capacitacaomatera.funcionario_service.entity;

import com.capacitacaomatera.funcionario_service.interfaces.IValidador;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.apache.commons.lang3.StringUtils;
import org.hibernate.annotations.Check;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.ValidationException;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Endereco implements IValidador {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 100)
    private String rua;

    @NotNull
    @Size(max = 100)
    private String bairro;

    @NotNull
    @Check(constraints = "numero > 0")
    private int numero;

    @NotNull
    @Size(max = 50)
    private String cidade;

    @NotNull
    @Size(max = 50)
    private String uf;

    public Endereco() {/*Model Mapper e Hbernate*/}

    public void setId(Long id) {
        this.id = id;
    }

    public void setRua(String rua) {
        this.rua = rua;
    }

    public void setBairro(String bairro) {
        this.bairro = bairro;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public Long getId() {
        return id;
    }

    public String getRua() {
        return rua;
    }

    public String getBairro() {
        return bairro;
    }

    public int getNumero() {
        return numero;
    }

    public String getCidade() {
        return cidade;
    }

    public String getUf() {
        return uf;
    }

    @Override
    public boolean validar() {
        if (StringUtils.isBlank(this.rua))
            throw new ValidationException("O nome da rua deve ser informado.");

        if (StringUtils.isBlank(this.bairro))
            throw new ValidationException("O nome do bairro deve ser informado.");

        if (this.numero == 0)
            throw new ValidationException("O número da residência deve ser informado.");

        if (this.numero < 0)
            throw new ValidationException("O número da residência deve ser um valor positivo.");

        if (StringUtils.isBlank(this.cidade))
            throw new ValidationException("O nome da cidade deve ser informado.");

        if (StringUtils.isBlank(this.uf))
            throw new ValidationException("O nome da cidade deve ser informado.");

        return true;
    }

    public static final class Builder {

        private String rua;
        private String bairro;
        private int numero;
        private String cidade;
        private String uf;

        public Builder withRua(String rua) {
            this.rua = rua;
            return this;
        }

        public Builder withBairro(String bairro) {
            this.bairro = bairro;
            return this;
        }

        public Builder withNumero(int numero) {
            this.numero = numero;
            return this;
        }

        public Builder withCidade(String cidade) {
            this.cidade = cidade;
            return this;
        }

        public Builder withUf(String uf) {
            this.uf = uf;
            return this;
        }

        public Endereco build() {
            Endereco e = new Endereco();

            e.rua = rua;
            e.bairro = bairro;
            e.numero = numero;
            e.cidade = cidade;
            e.uf = uf;

            return e;
        }
    }

}
