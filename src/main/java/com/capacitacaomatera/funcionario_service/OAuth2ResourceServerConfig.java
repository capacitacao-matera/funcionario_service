package com.capacitacaomatera.funcionario_service;

import com.capacitacaomatera.funcionario_service.properties.ResourceServerProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.provider.token.RemoteTokenServices;

@Configuration
@EnableResourceServer
@EnableConfigurationProperties(ResourceServerProperties.class)
public class OAuth2ResourceServerConfig extends ResourceServerConfigurerAdapter {

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
            .antMatchers(HttpMethod.GET,"/funcionarios")
                .access("#oauth2.hasScope('read')")
            .antMatchers(HttpMethod.DELETE, "/funcionarios")
                .access("#oauth2.hasScope('write')");
    }

    @Bean
    public RemoteTokenServices tokenService(ResourceServerProperties resourceServerProperties) {
        RemoteTokenServices tokenService = new RemoteTokenServices();
        tokenService.setCheckTokenEndpointUrl(resourceServerProperties.getCheckTokenEndpoint());
        tokenService.setClientId(resourceServerProperties.getClientId());
        tokenService.setClientSecret(resourceServerProperties.getClientSecret());
        return tokenService;
    }
}
