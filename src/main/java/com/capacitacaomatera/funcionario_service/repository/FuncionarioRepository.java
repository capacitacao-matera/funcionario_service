package com.capacitacaomatera.funcionario_service.repository;

import com.capacitacaomatera.funcionario_service.entity.Funcionario;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, Long> {
}
