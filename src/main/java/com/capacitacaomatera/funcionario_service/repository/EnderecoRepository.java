package com.capacitacaomatera.funcionario_service.repository;

import com.capacitacaomatera.funcionario_service.entity.Endereco;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EnderecoRepository extends JpaRepository<Endereco, Long> {
}
