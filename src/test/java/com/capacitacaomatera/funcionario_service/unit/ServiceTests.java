package com.capacitacaomatera.funcionario_service.unit;

import com.capacitacaomatera.funcionario_service.entity.Endereco;
import com.capacitacaomatera.funcionario_service.entity.Funcionario;
import com.capacitacaomatera.funcionario_service.exceptions.DuplicateEmployeeException;
import com.capacitacaomatera.funcionario_service.exceptions.ResourceNotFoundException;
import com.capacitacaomatera.funcionario_service.repository.FuncionarioRepository;
import com.capacitacaomatera.funcionario_service.service.FuncionarioService;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit4.SpringRunner;

import javax.validation.ValidationException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static junit.framework.TestCase.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@Import(UnitTestConfig.class)
public class ServiceTests {

    @Autowired
    FuncionarioService funcionarioService;

    @MockBean
    FuncionarioRepository funcionarioRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void inserirFuncionarioValidoTest() {
        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Cauã Anthony Nelson Jesus")
            .withUsuario("cauant")
            .withEmail("caua.anthony@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("531.582.395-40")
            .withRg("25.442.206-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        try {
            Mockito.when(funcionarioRepository.save(funcionario)).thenReturn(funcionario);
            funcionarioService.inserirFuncionario(funcionario);
        }
        catch (Exception e){
            fail("Inserir usuário válido e não duplicado não deve lançar exceção");
        }
    }

    @Test
    public void inserirFuncionarioDuplicadoTest() {
        thrown.expect(DuplicateEmployeeException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenReturn(funcionario);
        funcionarioService.inserirFuncionario(funcionario);

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(DataIntegrityViolationException.class);
        funcionarioService.inserirFuncionario(funcionario);

    }

    @Test
    public void listarTest() {
        final List<Funcionario> funcionarios = new ArrayList<>();

        funcionarios.add(new Funcionario.Builder()
            .withNome("Cauã Anthony Nelson Jesus")
            .withUsuario("cauant")
            .withEmail("caua.anthony@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("531.582.395-40")
            .withRg("25.442.206-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build()
        );

        funcionarios.add(new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build()
        );

        Mockito.when(funcionarioRepository.findAll()).thenReturn(funcionarios);

        final List<Funcionario> funcionariosRes = funcionarioService.listarFuncionarios();

        assertTrue("Listagem de funcionários deve retornar valores", funcionariosRes.size() > 0);
    }

    @Test
    public void getFuncionario() {
        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.findById(3L)).thenReturn(Optional.of(funcionario));

        final Funcionario f = funcionarioService.getFuncionario(3L);

        assertNotNull(f);
    }

    @Test
    public void getFuncionarioInexistente() {
        thrown.expect(ResourceNotFoundException.class);
        Mockito.doThrow(ResourceNotFoundException.class).when(funcionarioRepository).findById(3L);

        funcionarioService.getFuncionario(3L);
    }

    @Test
    public void listarVazioTest() {
        final List<Funcionario> funcionarios = new ArrayList<>();

        Mockito.when(funcionarioRepository.findAll()).thenReturn(funcionarios);

        final List<Funcionario> fs = funcionarioService.listarFuncionarios();

        assertTrue(fs.size() <= 0);
    }

    @Test
    public void removerFuncionarioTest()  {
        funcionarioService.removerFuncionario(1L);

        verify(funcionarioRepository).deleteById(eq(1L));
    }

    @Test
    public void removerFuncionarioInexistenteTest() {
        thrown.expect(ResourceNotFoundException.class);

        Mockito.doThrow(ResourceNotFoundException.class).when(funcionarioRepository).deleteById(1000L);

        funcionarioService.removerFuncionario(1000L);
    }

    @Test
    public void inserirNomeInvalidoTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirEmailInvalidoTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirUsuarioInvalidoTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirTelefoneInvalidoTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirCpfInvalidoTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirRgInvalidoTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirSenhaInvalidaTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirEnderecoInvalidoTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirRuaInvalidaTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirBairroInvalidoTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirNumeroInvalidoTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirCidadeInvalidaTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withUf("Pernambuco")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

    @Test
    public void inserirUfInvalidaTest() {
        thrown.expect(ValidationException.class);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .build()
            )
            .build();

        Mockito.when(funcionarioRepository.save(funcionario)).thenThrow(ValidationException.class);
        funcionarioService.inserirFuncionario(funcionario);
    }

}
