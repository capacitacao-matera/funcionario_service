package com.capacitacaomatera.funcionario_service.integration;

import com.capacitacaomatera.funcionario_service.entity.Funcionario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ActiveProfiles;

@Service
@ActiveProfiles("test")
public class UserDetailTestService implements UserDetailsService {

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return new FuncionarioTestPrincipal (new Funcionario.Builder()
            .withUsuario("teste")
            .withSenha(passwordEncoder.encode("teste"))
            .build());
    }
}
