package com.capacitacaomatera.funcionario_service.integration;

import com.capacitacaomatera.funcionario_service.service.FuncionarioService;
import com.capacitacaomatera.funcionario_service.service.FuncionarioServiceImpl;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;
import org.springframework.security.oauth2.provider.token.store.JdbcTokenStore;

@TestConfiguration
public class IntegrationTestConfig {

    @Bean
	public FuncionarioService getFuncionarioService() {
		return new FuncionarioServiceImpl();
	}

    @Bean
    public FuncionarioService getFuncionarioRepository() {
        return new FuncionarioServiceImpl();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder(11);
    }

}
