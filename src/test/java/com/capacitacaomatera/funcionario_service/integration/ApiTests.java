package com.capacitacaomatera.funcionario_service.integration;

import com.capacitacaomatera.funcionario_service.entity.Endereco;
import com.capacitacaomatera.funcionario_service.entity.Funcionario;
import com.capacitacaomatera.funcionario_service.utils.TestUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import static com.capacitacaomatera.funcionario_service.utils.TestUtils.toJson;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@TestPropertySource(locations="classpath:application-test.properties")
public class ApiTests {

    @Autowired
    private MockMvc mockMvc;

    private boolean setup = false;

    private String authorization = null;

    @Before
    public void setup() throws Exception {

        if (setup) return;

        this.authorization = TestUtils.obtainAccessToken("teste", "teste", mockMvc);

        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Carlos Ryan da Cruz")
            .withUsuario("carrya")
            .withEmail("carlos.ryan@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("646.928.522-83")
            .withRg("19.035.671-6")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        mockMvc.perform(
            post("/funcionarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(funcionario))
                .header("Authorization", "Bearer " + this.authorization)
        ).andDo(result -> setup = true);
    }

    @Test
    public void getFuncionario() throws Exception {
        mockMvc.perform(
            get("/funcionarios/1")
            .header("Authorization", "Bearer " + this.authorization)
        ).andExpect(status().isOk());
    }

    @Test
    public void listarFuncionarios() throws Exception {
        mockMvc.perform(
            get("/funcionarios")
            .header("Authorization", "Bearer " + this.authorization)
        ).andExpect(status().isOk());
    }

    @Test
    @Transactional
    @Rollback
    public void removerFuncionarioInexistente() throws Exception {
        mockMvc.perform(
            delete("/funcionarios/100")
            .header("Authorization", "Bearer " + this.authorization)
        ).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    @Rollback
    public void removerFuncionario() throws Exception {
        mockMvc.perform(
            delete("/funcionarios/1")
            .header("Authorization", "Bearer " + this.authorization)
        ).andExpect(status().isOk());
    }

    @Test
    public void getFuncionarioInexistente() throws Exception {
        mockMvc.perform(
            get("/funcionario/100")
            .header("Authorization", "Bearer " + this.authorization)
        ).andExpect(status().isNotFound());
    }

    @Test
    public void getNaoAutorizado() throws Exception {
        mockMvc.perform(
            get("/funcionario/100")
                .header("Authorization", "Bearer lorem_ipsum")
        ).andExpect(status().isUnauthorized());
    }

    @Test
    @Transactional
    @Rollback
    public void inserirFuncionarioDuplicado () throws Exception {
        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Lívia Isabelle Alessandra Mendes")
            .withUsuario("livisa")
            .withEmail("livia.isabelle@email.com")
            .withTelefone("(96) 2771-3232")
            .withCpf("597.574.203-01")
            .withRg("34.064.840-5")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        final String json = toJson(funcionario);

        mockMvc.perform(
            post("/funcionarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
                .header("Authorization", "Bearer " + this.authorization)
        ).andDo(result -> {
            mockMvc.perform(
                post("/funcionarios")
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(json)
                    .header("Authorization", "Bearer " + this.authorization)
            ).andExpect(status().isConflict());
        });

    }

    @Test
    @Transactional
    @Rollback
    public void inserirFuncionarioValido () throws Exception {
        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withUsuario("jossil")
            .withEmail("josefa.silvana@email.com")
            .withTelefone("(96) 99956-4700")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        mockMvc.perform(
            post("/funcionarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(funcionario))
                .header("Authorization", "Bearer " + this.authorization)
        ).andExpect(status().isOk());
    }

    @Test
    @Transactional
    @Rollback
    public void inserirFuncionarioInvalido () throws Exception {
        final Funcionario funcionario = new Funcionario.Builder()
            .withNome("Josefa Silvana Caldeira")
            .withEmail("josefa.silvana@email.com")
            .withCpf("351.209.211-08")
            .withRg("22.107.528-8")
            .withSenha("123456")
            .withEndereco(new Endereco.Builder()
                .withRua("Rua Campo Formoso")
                .withBairro("Vasco da Gama")
                .withNumero(661)
                .withCidade("Recife")
                .withUf("Pernambuco")
                .build()
            )
            .build();

        mockMvc.perform(
            post("/funcionarios")
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJson(funcionario))
                .header("Authorization", "Bearer " + this.authorization)
        ).andExpect(status().isBadRequest());
    }

}
