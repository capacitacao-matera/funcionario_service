FROM openjdk:8u212-jdk
WORKDIR /app
COPY target/funcionario_service.jar /app/
VOLUME /app
EXPOSE 8085/tcp
ENTRYPOINT [ "java", "-Dserver.port=8085", "-jar", "funcionario_service.jar"]